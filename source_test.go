package smtpfp

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
)

// Initialize a local repository, signing commits with the test key.
const repoSetupScript = `
git init . &&
git config user.email test@example.com
git config user.name test
git config user.signingkey F5724399F2F3220A
`

type testRepo struct {
	dir string
}

var privateGnupgHome string

func init() {
	cwd, _ := os.Getwd()
	privateGnupgHome = filepath.Join(cwd, "testdata/pgp-private")
}

// Run a command (using the shell) with the current directory set to
// the specified repository, and GNUPGHOME set to the GPG home dir
// containing the test secret key.
func runInTestRepo(dir, script string) error {
	cmd := exec.Command("/bin/sh", "-x", "-c", script)
	cmd.Dir = dir
	cmd.Env = []string{"GNUPGHOME=" + privateGnupgHome}
	//cmd.Stdout = os.Stdout
	//cmd.Stderr = os.Stderr
	return cmd.Run()
}

// Create a test repository with a signed commit (using the test
// secret key), and the specified contents, passed as a map of
// relative filename -> contents.
func newTestSignedRepo(contents map[string]string) (*testRepo, error) {
	dir, err := ioutil.TempDir("", "test_repo_")
	if err != nil {
		return nil, err
	}

	if err := runInTestRepo(dir, repoSetupScript); err != nil {
		return nil, err
	}

	var files []string
	for name, data := range contents {
		files = append(files, name)

		// If 'data' is a valid file name, use the contents of
		// it. Otherwise data represents the literal file
		// contents.
		if b, err := ioutil.ReadFile(data); err == nil {
			data = string(b)
		}

		// Ensure that the target directory exists.
		d := filepath.Dir(filepath.Join(dir, name))
		if _, err := os.Stat(d); err != nil {
			os.Mkdir(d, 0700)
		}

		if err := ioutil.WriteFile(filepath.Join(dir, name), []byte(data), 0600); err != nil {
			return nil, err
		}
		if err := runInTestRepo(dir, "git add "+name); err != nil {
			return nil, err
		}
	}

	if err := runInTestRepo(dir, "git commit -S -m first_commit"); err != nil {
		return nil, err
	}

	return &testRepo{dir}, nil
}

func (r *testRepo) Close() {
	os.RemoveAll(r.dir)
}

func TestSource_LoadKeys(t *testing.T) {
	g := &Source{
		Fingerprints: testFingerprints,
	}

	err := g.LoadKeys(loadTestKeyring(t))
	if err != nil {
		t.Fatal(err)
	}
}

func TestSource_LoadKeysFailure(t *testing.T) {
	g := &Source{
		Fingerprints: []string{badFingerprint},
	}

	err := g.LoadKeys(loadTestKeyring(t))
	if err == nil {
		t.Fatal("no error")
	}
}

func newTestRepoAndStorage(t *testing.T) (*testRepo, *Storage) {
	r, err := newTestSignedRepo(map[string]string{
		"test.yml": "test",
	})
	if err != nil {
		t.Fatal(err)
	}
	return r, NewTempStorage()
}

func TestSource_Download(t *testing.T) {
	r, storage := newTestRepoAndStorage(t)
	defer r.Close()
	defer storage.Close()

	s := &Source{
		URLs: []string{r.dir},
	}
	if err := storage.Download(s); err != nil {
		t.Fatal(err)
	}
	if !storage.HasService(s, "test") {
		t.Fatal("inconsistency: test service does not exist")
	}
}

func TestSource_Verify(t *testing.T) {
	r, storage := newTestRepoAndStorage(t)
	defer r.Close()
	defer storage.Close()

	s := &Source{
		URLs:         []string{r.dir},
		Fingerprints: testFingerprints,
	}
	if err := storage.Download(s); err != nil {
		t.Fatal(err)
	}

	if err := s.LoadKeys(loadTestKeyring(t)); err != nil {
		t.Fatal(err)
	}
	if err := s.verify(storage.Path(s, "test.yml")); err != nil {
		t.Fatal(err)
	}
}
