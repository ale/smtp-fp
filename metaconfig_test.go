package smtpfp

import (
	"bytes"
	"os"
	"path/filepath"

	"testing"
	"text/template"
	"time"
)

const (
	testValidMetaconfig = `
poivron.org:
  urls:
    - https://git.poivron.org/repository.git
    - http://git.poivron.org/repository.git
  fingerprints:
    - 1234567890123456789012345678901234567890
`

	testLocalMetaconfigTmpl = `
testsource:
  urls:
    - {{.RepoDir}}
  fingerprints:
    - {{.Fingerprint}}
`
)

func TestMetaConfig_New(t *testing.T) {
	cfg, err := NewMetaConfigFromBytes([]byte(testValidMetaconfig), nil, nil)
	if err != nil {
		t.Fatal(err)
	}
	if len(cfg.Sources) != 1 {
		t.Fatalf("unexpected results: %v", cfg)
	}
	g := cfg.Sources["poivron.org"]
	if g == nil {
		t.Fatalf("test source not found: %v", cfg)
	}
	if g.Name != "poivron.org" {
		t.Fatalf("bad Name: %v", g)
	}
	if len(g.URLs) != 2 {
		t.Errorf("bad urls: %v", g)
	}
	if len(g.Fingerprints) != 1 {
		t.Errorf("bad fingerprints: %v", g)
	}
}

// Create a meta configuration with a single source pointing at a local
// signed git repository.
func newLocalMetaConfig(t *testing.T, dir string, fingerprint string) *MetaConfig {
	tmpl, err := template.New("tmpl").Parse(testLocalMetaconfigTmpl)
	if err != nil {
		t.Fatal(err)
	}

	ctx := struct {
		RepoDir     string
		Fingerprint string
	}{dir, fingerprint}
	var b bytes.Buffer
	err = tmpl.Execute(&b, &ctx)
	if err != nil {
		t.Fatal(err)
	}

	storage := NewTempStorage()
	cfg, err := NewMetaConfigFromBytes(b.Bytes(), loadTestKeyring(t), storage)
	if err != nil {
		t.Fatal(err)
	}
	return cfg
}

// Create a test context so we can call Close() properly on everything.
type testMetaConfigContext struct {
	metaconf *MetaConfig
	repo     *testRepo
}

func (c testMetaConfigContext) Close() {
	c.metaconf.Close()
	c.repo.Close()
}

// Create a signed git repository with a single service ('smtp'), and
// the associated meta configuration.
func testLocalMetaConfig(t *testing.T, fingerprint string) (*MetaConfig, *testMetaConfigContext) {
	r, err := newTestSignedRepo(map[string]string{
		"smtp.yml":                   testValidServiceData,
		"certs/poivron.org-2013.pem": "testdata/certs/poivron.org-2013.pem",
		"certs/poivron.org-2014.pem": "testdata/certs/poivron.org-2014.pem",
		"certs/potager.org-2013.pem": "testdata/certs/potager.org-2013.pem",
		"certs/potager.org-2014.pem": "testdata/certs/potager.org-2014.pem",
	})
	if err != nil {
		t.Fatal(err)
	}
	metaconf := newLocalMetaConfig(t, r.dir, fingerprint)
	return metaconf, &testMetaConfigContext{metaconf, r}
}

func TestMetaConfig_Download(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, goodFingerprint)
	defer ctx.Close()

	if err := cfg.Download(); err != nil {
		t.Fatalf("Download() error: %v", err)
	}
}

func TestMetaConfig_DownloadFailure(t *testing.T) {
	cfg := newLocalMetaConfig(t, "/does/not/exist", goodFingerprint)
	defer cfg.Close()

	err := cfg.Download()
	if err == nil {
		t.Fatalf("Download() returned no error: %v", err)
	}
}

func TestMetaConfig_StoragePersistenceAcrossDownloadFailures(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, goodFingerprint)
	defer ctx.Close()

	source := cfg.Sources["testsource"]

	// First, download everything successfully and check that
	// HasService(smtp) is true.
	if err := cfg.Download(); err != nil {
		t.Fatalf("Download() error: %v", err)
	}
	if !cfg.storage.HasService(source, "smtp") {
		t.Fatal("no 'smtp' service for our test source")
	}

	// Tweak the configuration by injecting a faulty URL.
	source.URLs[0] = "/does/not/exist"

	// Download again, check the failure, and verify that
	// HasService is still true.
	if err := cfg.Download(); err == nil {
		t.Fatal("Download() did not fail")
	}
	if !cfg.storage.HasService(source, "smtp") {
		t.Fatal("smtp service disappeared")
	}
}

func TestMetaConfig_LoadKeys(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, goodFingerprint)
	defer ctx.Close()

	if err := cfg.LoadKeys(); err != nil {
		t.Fatalf("LoadKeys() errors: %v", err)
	}
}

func TestMetaConfig_LoadKeysFailure(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, badFingerprint)
	defer ctx.Close()

	err := cfg.LoadKeys()
	if err == nil {
		t.Fatalf("LoadKeys() returned no error: %v", err)
	}
}

func TestMetaConfig_GetService(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, goodFingerprint)
	defer ctx.Close()

	cfg.Download()
	cfg.LoadKeys()

	svcs := cfg.GetTLSServices("smtp")
	if len(svcs) == 0 {
		t.Fatal("no services for 'smtp'")
	}
}

func TestMetaConfig_SourceTTL(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, goodFingerprint)
	defer ctx.Close()

	cfg.Download()
	cfg.LoadKeys()

	svcs := cfg.GetTLSServices("smtp")
	if len(svcs) == 0 {
		t.Fatal("no services for 'smtp'")
	}

	// Fix the source TTL by manually tweaking the timestamp file.
	cfg.SourceTTL = 30 * time.Second
	old := time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC)
	os.Chtimes(filepath.Join(cfg.storage.Dir, "testsource", "stamp"), old, old)

	// Check that GetTLSServices now returns nothing.
	svcs = cfg.GetTLSServices("smtp")
	if len(svcs) != 0 {
		t.Fatalf("source did not expire: %v", svcs)
	}
}

func TestMetaConfig_Fingerprints(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, goodFingerprint)
	defer ctx.Close()

	cfg.Download()
	cfg.LoadKeys()

	// In our test data, there is just a single certificate for
	// each of the two test domains.
	fp := GetTLSFingerprints(cfg.GetTLSServices("smtp"))
	if len(fp) != 2 || len(fp["potager.org"]) != 1 {
		t.Fatalf("bad fingerprints: %v", fp)
	}
}

func TestMetaConfig_HiddenServices(t *testing.T) {
	cfg, ctx := testLocalMetaConfig(t, goodFingerprint)
	defer ctx.Close()

	cfg.Download()
	cfg.LoadKeys()

	// Our test data has 1 test domain with a hidden service.
	hs := GetHiddenServices(cfg.GetTLSServices("smtp"))
	if len(hs) != 1 || len(hs["example.org"]) != 1 {
		t.Fatalf("bad hidden services: %v", hs)
	}
}
