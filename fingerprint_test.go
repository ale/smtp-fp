package smtpfp

import (
	"testing"
)

const testGoodFingerprint = "a4:05:a9:6c:5b:de:88:52:5a:28:c9:e0:16:0e:17:76:fb:42:64:7d:9f:53:9e:cf:7b:55:a5:e8:b9:3c:54:c3"

func TestConfig_GetCertificateFingerprint(t *testing.T) {
	service, err := NewServiceFromBytes("testdata/config", []byte(testValidServiceData))
	if err != nil {
		t.Fatal(err)
	}

	crtPath := service.Config[0].Domains[0].Cert
	fp, err := service.GetCertificateFingerprint(crtPath)
	if err != nil {
		t.Fatal(err)
	}
	if want := testGoodFingerprint; fp != want {
		t.Fatalf("bad fingerprint: got=%s, want=%s", fp, want)
	}

	// Try a certificate that does not exist.
	_, err = service.GetCertificateFingerprint("nonexisting")
	if err == nil {
		t.Fatal("no error for non-existing certificate")
	}
}
