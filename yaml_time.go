package smtpfp

import "time"

// Time format for validity range specifications in YAML.
const dateFormat = "2006-01-02 15:04"

// A YAML-serializable time.Time type.
type YamlTime struct {
	time.Time
}

func (y YamlTime) GetYAML() (tag string, value interface{}) {
	return "", y.Format(dateFormat)
}

func (y *YamlTime) SetYAML(tag string, value interface{}) bool {
	y.Time, _ = time.Parse(dateFormat, value.(string))
	return true
}
