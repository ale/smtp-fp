package output

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strings"
)

// A Generator processes domain -> fingerprints and domain ->
// hidden_service maps, resulting in filesystem changes (returned as a
// filename -> contents map).
type Generator interface {
	Update(fingerprints, hiddenServices map[string][]string) (map[string]string, error)
}

// Apply takes a bunch of updates (a filename/content map) and writes
// them to the filesystem.
func Apply(updates map[string]string) error {
	for filename, contents := range updates {
		tmpf, err := ioutil.TempFile(path.Dir(filename), ".tmp_")
		if err != nil {
			return err
		}
		_, err = io.WriteString(tmpf, contents)
		if err != nil {
			tmpf.Close()
			os.Remove(tmpf.Name())
			return err
		}
		tmpf.Close()
		if err := os.Rename(tmpf.Name(), filename); err != nil {
			os.Remove(tmpf.Name())
			return err
		}
	}
	return nil
}

// Diff shows the difference between some updates (a filename/content
// map) and the current contents of those files on the filesystem.
func Diff(updates map[string]string) error {
	base, err := ioutil.TempDir("", "diff_")
	if err != nil {
		return err
	}
	defer os.RemoveAll(base)

	baseNew := path.Join(base, "new")
	baseOld := path.Join(base, "old")
	for _, d := range []string{baseNew, baseOld} {
		if err := os.Mkdir(d, 0700); err != nil {
			return err
		}
	}

	for filename, contents := range updates {
		for _, b := range []string{baseNew, baseOld} {
			d := path.Join(b, path.Dir(filename))
			os.MkdirAll(d, 0700)
		}
		if _, err := os.Stat(filename); err == nil {
			os.Symlink(filename, path.Join(baseOld, filename))
		}
		if err := ioutil.WriteFile(path.Join(baseNew, filename), []byte(contents), 0600); err != nil {
			return err
		}
	}

	cmd := exec.Command("diff", "-urNb", "old", "new")
	cmd.Dir = base
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

type generatorFactoryFunc func(string) (Generator, error)

var knownGenerators = map[string]generatorFactoryFunc{}

func RegisterGenerator(prefix string, factory func(string) (Generator, error)) {
	knownGenerators[prefix] = generatorFactoryFunc(factory)
}

// NewGenerator parses the given output spec and returns a Generator.
func NewGenerator(spec string) (Generator, error) {
	if !strings.Contains(spec, ":") {
		spec = "postfix:" + spec
	}
	specparts := strings.SplitN(spec, ":", 2)
	prefix := specparts[0]
	arg := specparts[1]
	gen, ok := knownGenerators[prefix]
	if !ok {
		return nil, fmt.Errorf("unknown generator spec '%s'", prefix)
	}
	return gen(arg)
}
