package output

import (
	"bytes"
	"errors"
	"fmt"
)

type postfixGenerator struct {
	filename string
}

func newPostfixGenerator(filename string) (Generator, error) {
	if filename == "" {
		return nil, errors.New("empty filename")
	}
	return &postfixGenerator{filename: filename}, nil
}

// The Postfix generator currently ignores hidden services.
func (g *postfixGenerator) Update(fingerprints, hiddenServices map[string][]string) (map[string]string, error) {
	var b bytes.Buffer

	for domain, fps := range fingerprints {
		fmt.Fprintf(&b, "%s\tfingerprint\n", domain)
		for _, fp := range fps {
			fmt.Fprintf(&b, "        match=%s\n", fp)
		}
		fmt.Fprintf(&b, "\n")
	}

	return map[string]string{
		g.filename: b.String(),
	}, nil
}

func init() {
	RegisterGenerator("postfix", newPostfixGenerator)
}
