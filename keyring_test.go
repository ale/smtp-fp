package smtpfp

import (
	"os/exec"
	"strings"
	"testing"
)

const (
	goodFingerprint = "5DD74B50814D045D8C0120B8F5724399F2F3220A"
	badFingerprint  = "1234567890123456789012345678901234567890"
)

// One of these is fake, but it should not cause the tests to fail.
var testFingerprints = []string{
	goodFingerprint,
	badFingerprint,
}

func loadTestKeyring(t *testing.T) KeyRing {
	keyring, err := ReadKeyRing("testdata/keyring")
	if err != nil {
		t.Fatal(err)
	}
	return keyring
}

func TestKeyring_GetKeysWithFingerprint(t *testing.T) {
	entities, err := loadTestKeyring(t).GetEntitiesWithFingerprint(testFingerprints)
	if err != nil {
		t.Fatal(err)
	}
	if len(entities) != 1 {
		t.Fatalf("bad entities: %v", entities)
	}

	// Test only the fake fp.
	_, err = loadTestKeyring(t).GetEntitiesWithFingerprint([]string{badFingerprint})
	if err == nil {
		t.Fatal("no error with bad fingerprint")
	}
}

func TestKeyring_Temp(t *testing.T) {
	keyring := loadTestKeyring(t)
	tkr, err := newTempKeyring(keyring)
	if err != nil {
		t.Fatal(err)
	}
	defer tkr.Close()

	cmd := tkr.Command(exec.Command("gpg", "--list-keys"))
	out, err := cmd.Output()
	t.Logf("gpg list-keys: %s", string(out))
	if err != nil {
		t.Fatal(err)
	}
}

func TestKeyring_TempEntities(t *testing.T) {
	keyring := loadTestKeyring(t)
	el, err := keyring.GetEntitiesWithFingerprint(testFingerprints)
	if err != nil {
		t.Fatal(err)
	}

	tkr, err := newTempKeyring(el)
	if err != nil {
		t.Fatal(err)
	}
	defer tkr.Close()

	// To verify that the temporary keyring actually contains our
	// test key, and that it's readable by gpg, we export it in
	// armored format. We have to check the output because gpg
	// will happily exit with status=0 even if the key is not found.
	cmd := tkr.Command(exec.Command("gpg", "--export", "-a", "F2F3220A"))
	out, err := cmd.Output()
	if err != nil {
		t.Fatal(err)
	}
	if !strings.HasPrefix(string(out), "-----BEGIN PGP PUBLIC KEY BLOCK") {
		t.Fatalf("bad output: %s", string(out))
	}
}
