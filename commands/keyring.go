package commands

import (
	"errors"
	"fmt"

	"git.autistici.org/ale/smtp-fp"
	"git.autistici.org/ale/smtp-fp/third_party/go-commander"
)

func doUpdateKeyring(config *smtpfp.MetaConfig) error {
	defer config.Close()

	// Load the users' keyring.
	userKeyring, err := smtpfp.ReadKeyRing(expandTilde("~/.gnupg/pubring.gpg"))
	if err != nil {
		return err
	}

	// Load the existing keyring file, or create it anew.
	var keyring smtpfp.KeyRing
	if KeyringFile != "" {
		keyring, err = smtpfp.ReadKeyRing(KeyringFile)
		if err != nil {
			keyring = make(smtpfp.KeyRing, 0)
		}
	}

	// Iterate over all the fingerprints that exist in the global
	// configuration.
	changed := false
	missing := false
	for _, source := range config.Sources {
		for _, fp := range source.Fingerprints {
			if el, err := keyring.GetEntitiesWithFingerprint([]string{fp}); err != nil || len(el) == 0 {
				userel, err := userKeyring.GetEntitiesWithFingerprint([]string{fp})
				if err != nil || len(userel) == 0 {
					fmt.Printf("missing key: %s\n", fp)
					missing = true
				} else if KeyringFile != "" {
					fmt.Printf("adding key %s\n", fp)
					keyring = append(keyring, userel...)
					changed = true
				}
			}
		}
	}

	// Update the keyring file if something has changed.
	if KeyringFile != "" && changed {
		fmt.Printf("updating %s\n", KeyringFile)
		keyring.Write(KeyringFile)
	}

	// Return a non-zero exit status if there are missing keys.
	if missing {
		return errors.New("there are missing keys")
	}
	fmt.Printf("all keys up to date\n")
	return nil
}

func newUpdateKeyring() *commander.Command {
	cmd := &commander.Command{
		UsageLine: "update-keyring",
		Short:     "update the global keyring",
		Long: `Update a keyring with all the keys required to validate the configuration.

Missing keys will be searched for in your personal keyring in
~/.gnupg. Use the --keyring option to create or update an existing
standalone keyring, otherwise the program will only report missing
keys in your personal keyring. You can use the GNUPGHOME environment
variable if your keyring is not in ~/.gnupg.
`,
		Run: func(cmd *commander.Command, args []string) error {
			if len(args) != 0 {
				logger.Fatal("too many arguments")
			}
			// Read the configuration without passing a
			// valid keyring, which is not necessary and
			// may not exist (yet).
			return doUpdateKeyring(readConfig(smtpfp.KeyRing{}))
		},
	}
	return cmd
}
