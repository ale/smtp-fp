package commands

import (
	"errors"
	"flag"

	"git.autistici.org/ale/smtp-fp"
	"git.autistici.org/ale/smtp-fp/third_party/go-commander"
)

var (
	updateDiff     bool
	outputSpecFlag string
	serviceName    string
)

func doUpdateService(config *smtpfp.MetaConfig, service, outputSpec string) error {
	defer config.Close()

	initConfig(config)

	if err := smtpfp.UpdateTLSService(config, service, outputSpec, updateDiff); err != nil {
		return err
	}
	return nil
}

func newUpdateService() *commander.Command {
	cmd := &commander.Command{
		UsageLine: "update-service --service=<service>",
		Short:     "update service maps",
		Long:      "Update maps for the specified service.",
		Flag:      &commander.FlagSet{flag.NewFlagSet("update", flag.ExitOnError)},
		Run: func(cmd *commander.Command, args []string) error {
			if len(args) != 0 {
				return errors.New("too many arguments")
			}
			if serviceName == "" {
				return errors.New("must specify --service")
			}
			keyring, err := readKeyring()
			if err != nil {
				return err
			}
			return doUpdateService(readConfig(keyring), serviceName, outputSpecFlag)
		},
	}

	cmd.Flag.StringVar(&serviceName, "service", "", "service to update")
	cmd.Flag.StringVar(&outputSpecFlag, "output", "postfix:tls_policy", "output spec (format is <handler:args>, the only known handler is currently 'postfix')")
	cmd.Flag.BoolVar(&updateDiff, "diff", false, "only show a diff, do not apply resulting changes")

	return cmd
}
