package commands

import (
	"errors"
	"flag"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"git.autistici.org/ale/smtp-fp"
	"git.autistici.org/ale/smtp-fp/third_party/go-commander"
)

var (
	// Global options.
	KeyringFile string
	ConfigFile  string
	StateDir    string
	SourceTTL   time.Duration

	// Repeat in this module for convenience.
	logger = smtpfp.RootLogger
)

func expandTilde(s string) string {
	if strings.HasPrefix(s, "~/") {
		return filepath.Join(os.Getenv("HOME"), strings.TrimPrefix(s, "~/"))
	}
	return s
}

// Return the default filename for the keyring associated with
// ConfigFile.
func defaultKeyringPath() string {
	return filepath.Join(filepath.Dir(ConfigFile), "keyring")
}

// Try a few possibilities until we find a suitable PGP keyring.
func readKeyring() (smtpfp.KeyRing, error) {
	// Build the list of candidates. Order is very important: user
	// preferences should take precedence over defaults.
	var files []string
	if KeyringFile != "" {
		files = append(files, KeyringFile)
	}
	if gpgHome := os.Getenv("GNUPGHOME"); gpgHome != "" {
		files = append(files, filepath.Join(gpgHome, "pubring.gpg"))
	}
	files = append(files, defaultKeyringPath())
	files = append(files, expandTilde("~/.gnupg/pubring.gpg"))

	// Ignore files that do not exist, display an error if something
	// goes wrong trying to load the ones that do.
	for _, f := range files {
		if _, err := os.Stat(f); os.IsNotExist(err) {
			continue
		}
		if keyring, err := smtpfp.ReadKeyRing(f); err != nil {
			logger.Errorf("%s: %v", f, err)
		} else {
			return keyring, nil
		}
	}
	return nil, errors.New("no keyring was found")
}

// Create a new MetaConfig using command-line flags.
func readConfig(keyring smtpfp.KeyRing) *smtpfp.MetaConfig {
	if ConfigFile == "" {
		logger.Fatal("must specify --config")
	}

	data, err := ioutil.ReadFile(ConfigFile)
	if err != nil {
		logger.Fatal(err)
	}

	var storage *smtpfp.Storage
	if StateDir == "" {
		// Create a temporary storage for the local
		// repositories, that will be wiped when the program
		// exits.
		storage = smtpfp.NewTempStorage()
	} else {
		storage = smtpfp.NewStorage(StateDir)
	}
	cfg, err := smtpfp.NewMetaConfigFromBytes(data, keyring, storage)
	if err != nil {
		logger.Fatal(err)
	}
	cfg.SourceTTL = SourceTTL

	return cfg
}

// Initialize a MetaConfig, loading PGP keys and downloading all the
// sources. An error at this stage is not fatal unless *no* valid
// sources could be found.
func initConfig(config *smtpfp.MetaConfig) {
	// Setup the PGP keys.
	if err := config.LoadKeys(); err != nil {
		logger.Error(err)
	}

	// Attempt to download sources.
	if err := config.Download(); err != nil {
		if config.IsFullFailure(err) {
			logger.Fatalf("no sources could be updated: %v", err)
		} else {
			logger.Error(err)
		}
	}
}

// Main binary entry point.
func Run() error {
	var debug bool
	toplevel := &commander.Commander{
		Name: "smtpfp",
		Flag: &commander.FlagSet{flag.NewFlagSet("smtpfp", flag.ExitOnError)},
		Commands: []*commander.Command{
			newUpdate(),
			newUpdateKeyring(),
			newUpdateService(),
			newPreCommit(),
		},
		PostRunHook: func() {
			if debug {
				smtpfp.RootLogger.Level = smtpfp.INFO
			}
		},
	}

	toplevel.Flag.StringVar(&ConfigFile, "config", "", "top-level configuration file")
	toplevel.Flag.StringVar(&KeyringFile, "keyring", "", "GPG keyring (defaults to ~/.gnupg/pubring.gpg)")
	toplevel.Flag.StringVar(&StateDir, "state-dir", "", "state directory - if set, repositories will persist between invocations (making subsequent runs faster)")
	toplevel.Flag.DurationVar(&SourceTTL, "source-ttl", 7*24*time.Hour, "how long a source should be still considered valid without receiving a successful update")
	toplevel.Flag.BoolVar(&debug, "debug", false, "show debug messages")
	toplevel.Flag.BoolVar(&debug, "v", false, "show debug messages")

	return toplevel.Run(os.Args[1:])
}
