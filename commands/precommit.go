package commands

import (
	"errors"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"git.autistici.org/ale/smtp-fp"
	"git.autistici.org/ale/smtp-fp/third_party/go-commander"
)

const gitConfigVar = "hooks.gfp.fingerprint"

// Return the git config value for 'hooks.gfp.fingerprint'.
func gitConfigFingerprint() string {
	out, err := exec.Command("git", "config", "--get", gitConfigVar).Output()
	fp := strings.TrimSpace(string(out))
	if err != nil || fp == "" {
		logger.Fatalf("Configure git with 'git config %s <PGP_FINGERPRINT>", gitConfigVar)
	}
	return fp
}

// Pre-commit hook: synthesize a Source out of the local git
// repository, and run Validate on all the discovered services.
func doPreCommit(args []string) error {
	logger.Info("running pre-commit checks...")

	cwd, err := os.Getwd()
	if err != nil {
		logger.Fatal(err)
	}

	// Create a local source that does not attempt to verify
	// existing commit signatures, this is the pre-commit stage so
	// a new one will be generated soon anyway.
	source := smtpfp.NewSourceWithLocalRepository(cwd)

	// Attempt to discover services by looking at files with a
	// .yml extension in the local repository. This works as long
	// as everything is of type TLSService...
	files, err := filepath.Glob("*.yml")
	if err != nil {
		logger.Fatal(err)
	}
	if len(files) == 0 {
		logger.Fatal("No services defined")
	}

	// Invoking GetService() on our local sources will trigger
	// validation, and fail if something is wrong.
	errs := 0
	for _, filename := range files {
		var s smtpfp.TLSService
		if err := source.GetService(filename, &s); err != nil {
			logger.Errorf("%s: %v", filename, err)
			errs++
		} else {
			logger.Infof("%s: ok", filename)
		}
	}
	if errs > 0 {
		return errors.New("there have been validation errors, aborting commit")
	}
	return nil
}

func newPreCommit() *commander.Command {
	return &commander.Command{
		UsageLine: "pre-commit",
		Short:     "pre-commit hook for validation",
		Long:      "Run validation checks before committing changes to your local git repository.",
		Run: func(cmd *commander.Command, args []string) error {
			return doPreCommit(args)
		},
	}
}
