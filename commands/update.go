package commands

import (
	"errors"
	"flag"
	"io/ioutil"

	"git.autistici.org/ale/smtp-fp"
	"git.autistici.org/ale/smtp-fp/third_party/go-commander"
	"gopkg.in/yaml.v1"
)

type SystemService struct {
	Spec string
}

type SystemConfig struct {
	Services map[string]*SystemService
}

func loadSystemConfig(filename string) (*SystemConfig, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	var sys SystemConfig
	if err := yaml.Unmarshal(data, &sys.Services); err != nil {
		return nil, err
	}
	return &sys, nil
}

func doUpdate(config *smtpfp.MetaConfig, layout string) error {
	defer config.Close()

	// Load the system configuration.
	sys, err := loadSystemConfig(layout)
	if err != nil {
		logger.Fatal(err)
	}

	initConfig(config)

	errs := 0
	for name, service := range sys.Services {
		if err := smtpfp.UpdateTLSService(config, name, service.Spec, updateDiff); err != nil {
			logger.Warningf("%v", err)
			errs++
		}
	}
	if errs > 0 {
		return errors.New("there were errors")
	}
	return nil
}

func newUpdate() *commander.Command {
	var layout string

	cmd := &commander.Command{
		UsageLine: "update --layout=<system.yml>",
		Short:     "update all service maps",
		Long:      "Update all known maps for the local system.",
		Flag:      &commander.FlagSet{flag.NewFlagSet("update", flag.ExitOnError)},
		Run: func(cmd *commander.Command, args []string) error {
			if len(args) != 0 {
				return errors.New("too many arguments")
			}
			if layout == "" {
				return errors.New("must specify --layout")
			}
			keyring, err := readKeyring()
			if err != nil {
				return err
			}
			return doUpdate(readConfig(keyring), layout)
		},
	}

	cmd.Flag.StringVar(&layout, "layout", "layout.yml", "local system service layout map")
	cmd.Flag.BoolVar(&updateDiff, "diff", false, "only show a diff, do not apply resulting changes")

	return cmd
}
