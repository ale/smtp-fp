package smtpfp

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"gopkg.in/yaml.v1"
)

// Source holds the repository configuration for an organization
// providing services. Service configuration will be downloaded from
// one of the specified repositories, whose commits must be signed by
// a key with one of the specified fingerprints.
type Source struct {
	// Source name. This is not loaded from YAML but set manually
	// after deserialization.
	Name string `yaml:"-"`

	// Repository URLs, in preference order.
	URLs []string

	// Fingerprints of the keys that are allowed to commit to this
	// repository.
	Fingerprints []string

	skipVerify bool
	keys       KeyRing
}

// LoadKeys finds the repository keys in the keyring. Invalid
// fingerprints are ignored, but an error is returned if no matching
// fingerprints are found in the keyring.
func (s *Source) LoadKeys(keyring KeyRing) error {
	var err error
	s.keys, err = keyring.GetEntitiesWithFingerprint(s.Fingerprints)
	return err
}

// GetService deserializes the configuration for the specified service
// given the path to the configuration file.
func (s *Source) GetService(filename string, out interface{}) error {
	// Verify that the last commit for the service file is signed
	// by a known key.
	if !s.skipVerify {
		if err := s.verify(filename); err != nil {
			return err
		}
	}

	// Deserialize data from the YAML file.
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(data, out); err != nil {
		return err
	}

	// If the output type has a SetFile() method, call it (it is
	// used to make TLSService aware of relative paths).
	if f, ok := out.(hasSetFile); ok {
		f.SetFile(filename)
	}

	// Validate the resulting Service if it matches the hasValidate
	// interface.
	if v, ok := out.(hasValidate); ok {
		if err := v.Validate(); err != nil {
			return fmt.Errorf("validation error: %v", err)
		}
	}

	return nil
}

// Verify that the last commit for 'filename' has been signed by one
// of the authorized PGP keys.
func (s *Source) verify(filename string) error {
	if len(s.keys) == 0 {
		return errors.New("no PGP keys")
	}

	// Create a temporary keyring for GPG so that we can delegate
	// signature verification to it (via git).
	tmpKeyring, err := newTempKeyring(s.keys)
	if err != nil {
		return err
	}
	defer tmpKeyring.Close()

	// Check that git verifies the last commit successfully using
	// one of the keys in the temporary keyring.
	return verifyLastCommit(filename, tmpKeyring)
}

// The Storage contains the most recent permanent copies of the data
// downloaded from the Sources. Since each source can have multiple
// repositories, we keep a separate copy of every one of them and use
// a symbolic link to point at the active one (so that configuration
// readers do not need to know about the Source download status).
//
// This is the resulting directory hierarchy:
//
// root
// \---- source_name
//    +---- repo1
//    +---- repo2
//    +---- current -> repo2
//    \---- stamp
//
type Storage struct {
	Dir       string
	temporary bool
}

func NewStorage(dir string) *Storage {
	return &Storage{Dir: dir}
}

func NewTempStorage() *Storage {
	dir, err := ioutil.TempDir("", "storage_")
	if err != nil {
		panic(err)
	}
	return &Storage{Dir: dir, temporary: true}
}

// Close frees all resources associated with the Storage.
func (s *Storage) Close() {
	if s.temporary {
		os.RemoveAll(s.Dir)
	}
}

// Path returns the path to 'filename' relative to the currently
// active repository for Source.
func (s *Storage) Path(source *Source, filename string) string {
	return filepath.Join(s.Dir, source.Name, "current", filename)
}

// Set the currently active local repository.
func (s *Storage) setCurrent(source *Source, repoName string) error {
	current := filepath.Join(s.Dir, source.Name, "current")
	// Remove the current symlink if present.
	if _, err := os.Stat(current); err == nil {
		if err := os.Remove(current); err != nil {
			return err
		}
	}
	return os.Symlink(repoName, current)
}

// Update the timestamp of the last successful update for Source.
func (s *Storage) setStamp(source *Source, stamp time.Time) error {
	file, err := os.OpenFile(filepath.Join(s.Dir, source.Name, "stamp"), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer file.Close()
	fmt.Fprintf(file, "%d\n", stamp.Unix())
	return nil
}

// GetStamp returns the timestamp of the last successful update of the
// specified Source.
func (s *Storage) GetStamp(source *Source) (time.Time, error) {
	stat, err := os.Stat(filepath.Join(s.Dir, source.Name, "stamp"))
	if err != nil {
		return time.Now(), err
	}
	return stat.ModTime(), nil
}

// Download attempts to retrieve a source configuration from their git
// repositories. It tries the defined repositories in turn until one
// of them succeeds. An error is returned if no sources could be
// contacted successfully.
func (s *Storage) Download(source *Source) error {
	var merr multierr

	// Make sure that the top-level directory exists.
	if err := os.MkdirAll(filepath.Join(s.Dir, source.Name), 0700); err != nil {
		return err
	}

	for _, url := range source.URLs {
		// Checkout each URL into its own directory, so that
		// we can run 'git fetch' independently on subsequent
		// runs.
		repoName := normalizeUrl(url)
		dir := filepath.Join(s.Dir, source.Name, repoName)
		logger.Infof("updating repository %s", url)
		if err := updateGitRepository(url, dir); err != nil {
			logger.Warningf("%s: error checking out %s: %v", source.Name, url, err)
			merr.Add(url, err)
		} else {
			// This is now the active repository.
			if err := s.setCurrent(source, repoName); err != nil {
				return err
			}
			if err := s.setStamp(source, time.Now()); err != nil {
				return err
			}
			return nil
		}
	}
	return &merr
}

// HasService returns True if we have a configuration for the
// specified service and Source (either recent, or not).
func (s *Storage) HasService(source *Source, name string) bool {
	if fi, err := os.Stat(s.Path(source, name+".yml")); err == nil && fi.Mode().IsRegular() {
		return true
	}
	return false
}

// GetService deserializes the configuration for the specified service
// and Source.
func (s *Storage) GetService(source *Source, name string, out interface{}) error {
	return source.GetService(s.Path(source, name+".yml"), out)
}

// hasSetFile is an interface for types that have a SetFile method.
type hasSetFile interface {
	SetFile(string)
}

// hasValidate is an interface for types that have a Validate method.
type hasValidate interface {
	Validate() error
}

// NewSourceWithLocalRepository builds a Source out of a local git repo,
// it's used by the pre-commit hook. It needs to skip verification
// since there are no commits to verify yet.
func NewSourceWithLocalRepository(dir string) *Source {
	return &Source{
		Name:       "local",
		skipVerify: true,
	}
}

var nonAlnum = regexp.MustCompile(`[^[:alnum:]]+`)

// Strip non-alphanumeric characters, returning a string suitable for
// use as a path component.
func normalizeUrl(url string) string {
	return strings.Trim(nonAlnum.ReplaceAllLiteralString(url, "_"), "_")
}

// Return a command with a different working directory.
func withCwd(cwd string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = cwd
	return cmd
}

// Run a command, logging its stdout at INFO level. Standard error is
// always redirected to our own stderr.
func runCmd(cmd *exec.Cmd) error {
	var outbuf bytes.Buffer
	cmd.Stdout = &outbuf
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if output := strings.TrimSpace(outbuf.String()); output != "" {
		logger.Info(output)
	}
	return err
}

// Clone a repository, or update the existing local copy.
func updateGitRepository(url, dir string) error {
	if fi, err := os.Stat(dir); err == nil && fi.IsDir() {
		if err := runCmd(withCwd(dir, exec.Command("git", "fetch"))); err != nil {
			return fmt.Errorf("error running git fetch: %v", err)
		}
		if err := runCmd(withCwd(dir, exec.Command("git", "reset", "--hard", "HEAD"))); err != nil {
			return fmt.Errorf("error running git reset: %v", err)
		}
	} else {
		if err := runCmd(exec.Command("git", "clone", url, dir)); err != nil {
			return fmt.Errorf("error running git clone: %v", err)
		}
	}
	return nil
}

// Return the GPG signature status of the most recent commit for
// 'filename', as detected by 'git'. GPG will use the keys in the
// temporary keyring for validation. If no errors are returned,
// signature validation was successful.
func verifyLastCommit(filename string, t *tempKeyring) error {
	// Request both the single-character signature status and the
	// full GPG output from git. We only use the former to
	// evaluate verification status, but the full output is
	// returned to the caller if there is an error.
	data, err := t.Command(withCwd(filepath.Dir(filename), exec.Command("git", "log", "-n", "1", "--pretty=format:%G? %GG", "HEAD", filepath.Base(filename)))).Output()
	if err != nil {
		return fmt.Errorf("error running git log: %v", err)
	}
	c := string(data[0])
	// G: Good, U: Untrusted. Untrusted is included because we
	// don't copy the trustdb to the temporary GPG homedir,
	// counting on the implied trust given by the configuration
	// infrastructure and the git commit signatures.
	if c == "G" || c == "U" {
		return nil
	}
	return fmt.Errorf("signature validation failure:\n%s", string(data[2:len(data)]))
}
