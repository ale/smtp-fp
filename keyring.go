package smtpfp

import (
	"encoding/hex"
	"errors"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/packet"
)

type KeyRing openpgp.EntityList

func ReadKeyRing(filename string) (KeyRing, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	el, err := openpgp.ReadKeyRing(file)
	if err != nil {
		return nil, err
	}
	return KeyRing(el), nil
}

func (k KeyRing) Write(filename string) error {
	tempFile, err := ioutil.TempFile(filepath.Dir(filename), "keyring_")
	if err != nil {
		return err
	}
	defer tempFile.Close()

	for _, e := range k {
		err = e.Serialize(tempFile)
		if err != nil {
			os.Remove(tempFile.Name())
			return err
		}
	}
	tempFile.Close()
	err = os.Rename(tempFile.Name(), filename)
	if err != nil {
		os.Remove(tempFile.Name())
	}
	return err
}

func parseFingerprint(fp string) ([20]byte, error) {
	var fpb [20]byte
	b, err := hex.DecodeString(fp)
	if err != nil {
		return fpb, err
	}
	if len(b) != 20 {
		return fpb, errors.New("bad fingerprint")
	}
	copy(fpb[:], b)
	return fpb, nil
}

func (k KeyRing) getKeyWithFingerprint(fp [20]byte) *packet.PublicKey {
	for _, e := range k {
		if len(e.Revocations) > 0 {
			continue
		}
		if e.PrimaryKey.Fingerprint == fp {
			return e.PrimaryKey
		}
		for _, sk := range e.Subkeys {
			if sk.PublicKey.Fingerprint == fp {
				return sk.PublicKey
			}
		}
	}
	return nil
}

func (k KeyRing) GetKeysWithFingerprint(fingerprints []string) ([]*packet.PublicKey, error) {
	// We'd like to find at least one good fingerprint in the
	// keyring, among all those passed in the 'fingerprints'
	// argument. Try to return a meaningful error in that case.
	var keys []*packet.PublicKey
	lastErr := errors.New("key not found")
	for _, fpstr := range fingerprints {
		fp, err := parseFingerprint(fpstr)
		if err != nil {
			logger.Warningf("bad fingerprint '%s'", fpstr)
			lastErr = err
			continue
		}
		key := k.getKeyWithFingerprint(fp)
		if key == nil {
			continue
		}
		keys = append(keys, key)
	}
	if len(keys) == 0 {
		return nil, lastErr
	}

	return keys, nil
}

func (k KeyRing) getEntityWithFingerprint(fp [20]byte) *openpgp.Entity {
	for _, e := range k {
		if len(e.Revocations) > 0 {
			continue
		}
		if e.PrimaryKey.Fingerprint == fp {
			return e
		}
		for _, sk := range e.Subkeys {
			if sk.PublicKey.Fingerprint == fp {
				return e
			}
		}
	}
	return nil
}

// GetEntitiesWithFingerprint returns a KeyRing containing only the
// keys in 'k' that match one of the given fingerprints. An error is
// returned if no matches are found.
func (k KeyRing) GetEntitiesWithFingerprint(fingerprints []string) (KeyRing, error) {
	var entities openpgp.EntityList
	for _, fpstr := range fingerprints {
		if fp, err := parseFingerprint(fpstr); err == nil {
			if ent := k.getEntityWithFingerprint(fp); ent != nil {
				entities = append(entities, ent)
			}
		} else {
			logger.Warningf("bad fingerprint '%s'", fpstr)
		}
	}
	if len(entities) == 0 {
		return nil, errors.New("no matching fingerprints")
	}
	return KeyRing(entities), nil
}

type tempKeyring struct {
	tmpDir  string
	gpgHome string
}

func newTempKeyring(keyring KeyRing) (*tempKeyring, error) {
	base, err := ioutil.TempDir("", "temp_keyring_")
	if err != nil {
		return nil, err
	}
	tkr := &tempKeyring{
		tmpDir:  base,
		gpgHome: filepath.Join(base, ".gnupg"),
	}
	if err := os.Mkdir(tkr.gpgHome, 0700); err != nil {
		return nil, err
	}
	if err := keyring.Write(filepath.Join(tkr.gpgHome, "pubring.gpg")); err != nil {
		return nil, err
	}

	gpgout, _ := tkr.Command(exec.Command("gpg", "--list-keys")).Output()
	logger.Infof("created temporary keyring with %d keys: %s", len(keyring), gpgout)

	return tkr, nil
}

func (t *tempKeyring) Close() {
	os.RemoveAll(t.tmpDir)
}

func (t *tempKeyring) Command(cmd *exec.Cmd) *exec.Cmd {
	cmd.Env = []string{
		"GNUPGHOME=" + t.gpgHome,
	}
	return cmd
}
