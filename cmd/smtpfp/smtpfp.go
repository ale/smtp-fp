package main

import (
	"log"

	"git.autistici.org/ale/smtp-fp/commands"
)

func main() {
	if err := commands.Run(); err != nil {
		log.Fatal(err)
	}
}
