package smtpfp

import (
	"sync"
	"time"

	"gopkg.in/yaml.v1"
)

const downloadConcurrency = 5

// A MetaConfig groups together all the known Sources, and makes it
// possible to retrieve and validate their configurations. The source
// information is usually read from a YAML file, but the MetaConfig
// also needs a PGP keyring in order to verify source authenticity.
type MetaConfig struct {
	// Available sources.
	Sources map[string]*Source

	// How long after a source is considered stale without
	// successful updates.
	SourceTTL time.Duration

	keyring KeyRing
	storage *Storage
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Download all the known repositories, if possible. Individual errors
// are reported separately for each source. Downloads are parallelized
// with at most 'downloadConcurrency' parallel fetches.
func (g *MetaConfig) Download() error {
	inCh := make(chan *Source)
	type output struct {
		name string
		err  error
	}
	outCh := make(chan output)
	var wg sync.WaitGroup

	// Start a sufficient number of workers (this is an instance
	// of the standard scatter-gather pattern).
	for i := 0; i < min(downloadConcurrency, len(g.Sources)); i++ {
		wg.Add(1)
		go func() {
			for source := range inCh {
				if err := g.storage.Download(source); err != nil {
					logger.Errorf("update failed for %s: %v", source.Name, err)
					outCh <- output{source.Name, err}

					// If the source is stale,
					// warn the user now (better
					// to do it here than in
					// GetTLSServices, so that a
					// single message is generated
					// per source).
					if stamp, err := g.storage.GetStamp(source); err == nil && time.Now().Sub(stamp) > g.SourceTTL {
						logger.Warningf("source %s has not been updated successfully in a while", source.Name)
					}
				}
			}
			wg.Done()
		}()
	}

	// Inject the inputs from a separate goroutine, then close the
	// channels when done, causing the workers to quit. The final
	// close() will cause the gather loop to quit as well.
	go func() {
		for _, source := range g.Sources {
			inCh <- source
		}
		close(inCh)
		wg.Wait()
		close(outCh)
	}()

	// Gather the results.
	var merr multierr
	for result := range outCh {
		merr.Add(result.name, result.err)
	}
	if merr.Len() > 0 {
		return &merr
	}
	return nil
}

// LoadKeys attempts to load all the known PGP keys, and associate
// them with every known Source.
func (g *MetaConfig) LoadKeys() error {
	var merr multierr
	for _, source := range g.Sources {
		if err := source.LoadKeys(g.keyring); err != nil {
			merr.Add(source.Name, err)
		}
	}
	if merr.Len() > 0 {
		return &merr
	}
	return nil
}

// IsFullFailure returns true if the given error represents a total
// failure to update any sources.
func (g *MetaConfig) IsFullFailure(err error) bool {
	if e, ok := err.(*multierr); ok {
		return e.Len() == len(g.Sources)
	}
	return false
}

// GetTLSServices returns a global view of the named TLS service across
// all sources. Only call after LoadKeys (and, possibly, Download).
// The function operates on the existing contents of Storage, whether
// Download has been successful or not.
func (g *MetaConfig) GetTLSServices(service string) []*TLSService {
	now := time.Now()
	var services []*TLSService
	for _, source := range g.Sources {
		// Ignore missing services.
		if !g.storage.HasService(source, service) {
			continue
		}

		// Verify that the source is not stale.
		if stamp, err := g.storage.GetStamp(source); err != nil {
			// An error here is odd, ignore it.
			continue
		} else if now.Sub(stamp) > g.SourceTTL {
			// Stale source, ignore it.
			continue
		}

		var s TLSService
		if err := g.storage.GetService(source, service, &s); err != nil {
			logger.Errorf("error in service %s for source %s: %v", service, source.Name, err)
			continue
		}
		services = append(services, &s)
	}
	return services
}

func (g *MetaConfig) Close() {
	g.storage.Close()
}

// NewMetaConfigFromBytes creates a new MetaConfig.
func NewMetaConfigFromBytes(data []byte, keyring KeyRing, storage *Storage) (*MetaConfig, error) {
	conf := &MetaConfig{
		keyring:   keyring,
		storage:   storage,
		SourceTTL: 7 * 86400 * time.Second,
	}
	if err := yaml.Unmarshal(data, &conf.Sources); err != nil {
		return nil, err
	}

	// Set Source.Name on all sources.
	for name, s := range conf.Sources {
		s.Name = name
	}

	return conf, nil
}
