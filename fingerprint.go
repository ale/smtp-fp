package smtpfp

import (
	"fmt"

	"launchpad.net/gocert"
)

// GetCertificateFingerprint returns the SHA1 fingerprint of the
// public key of the X509 certificate stored in the specified file.
func (s *TLSService) GetCertificateFingerprint(relPath string) (string, error) {
	certs, err := gocert.LoadX509Pem(s.getCertificatePath(relPath))
	if err != nil {
		return "", fmt.Errorf("loading %s: %v", relPath, err)
	}
	if len(certs) > 1 {
		return "", fmt.Errorf("%s contains more than one certificate", relPath)
	}

	fp := gocert.SpkiFingerprint(certs[0])
	return fp.String(), nil
}
