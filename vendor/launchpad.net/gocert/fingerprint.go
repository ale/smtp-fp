/*
   This file is part of gocert.

   gocert is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, version 3.

   gocert is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with gocert.  If not, see <http://www.gnu.org/licenses/>.
*/

package gocert

import (
	"bytes"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"fmt"
	"strings"
)

// Fingerprint represents a cryptographically strong unique
// identifier of a public key identity.
type Fingerprint []byte

// String represents the fingerprint digest as a series of
// colon-delimited hexadecimal octets.
func (f Fingerprint) String() string {
	var buf bytes.Buffer
	for i, b := range f {
		if i > 0 {
			fmt.Fprintf(&buf, ":")
		}
		fmt.Fprintf(&buf, "%02x", b)
	}
	return buf.String()
}

// ParseFingerprint parses a colon-delimited series of hexadecimal octets.
func ParseFingerprint(fp string) (Fingerprint, error) {
	s := strings.Join(strings.Split(fp, ":"), "")
	buf, err := hex.DecodeString(s)
	return Fingerprint(buf), err
}

// SpkiFingerprint calculates a SHA256 digest of the SubjectPublicKeyInfo
// section of an X.509 certificate.
func SpkiFingerprint(cert *x509.Certificate) Fingerprint {
	h := sha256.New()
	h.Write(cert.RawSubjectPublicKeyInfo)
	return Fingerprint(h.Sum(nil))
}
