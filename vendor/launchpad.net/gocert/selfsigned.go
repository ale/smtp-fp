/*
   This file is part of gocert.

   gocert is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, version 3.

   gocert is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with gocert.  If not, see <http://www.gnu.org/licenses/>.
*/

package gocert

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	_ "crypto/sha512"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"log"
	"math/big"
	"os"
	"time"
)

func selfSignedTemplate(dn *pkix.Name) (template *x509.Certificate, err error) {
	notBefore := time.Now()
	notAfter := time.Date(2030, 12, 31, 23, 59, 59, 0, time.UTC)
	template = &x509.Certificate{
		SerialNumber: new(big.Int).SetInt64(0),
		Subject:      *dn,
		NotBefore:    notBefore,
		NotAfter:     notAfter,
		KeyUsage:     x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
	}
	return template, nil
}

type KeyOptions struct {
	Length int
	Type   KeyType
}

type KeyType uint8

const RsaKeyType KeyType = 0
const EcdsaKeyType KeyType = 1

func defaultOptions() *KeyOptions {
	return &KeyOptions{521, EcdsaKeyType}
}

// CreateDefaultKey creates a private key with specified options.
func CreateKey(keyFile string, options *KeyOptions) (privKey interface{}, err error) {
	if options == nil {
		options = defaultOptions()
	}
	var der []byte
	var keyType string
	switch options.Type {
	case RsaKeyType:
		rsaKey, err := rsa.GenerateKey(rand.Reader, options.Length)
		if err != nil {
			return nil, err
		}
		err = rsaKey.Validate()
		if err != nil {
			return nil, err
		}
		der = x509.MarshalPKCS1PrivateKey(rsaKey)
		privKey = rsaKey
		keyType = "RSA"
	case EcdsaKeyType:
		var curve elliptic.Curve
		switch options.Length {
		case 224:
			curve = elliptic.P224()
		case 256:
			curve = elliptic.P256()
		case 384:
			curve = elliptic.P384()
		case 521:
			curve = elliptic.P521()
		default:
			return nil, fmt.Errorf("Invalid ECDSA key length: %d", options.Length)
		}
		ecdsaKey, err := ecdsa.GenerateKey(curve, rand.Reader)
		if err != nil {
			return nil, err
		}
		der, err = x509.MarshalECPrivateKey(ecdsaKey)
		if err != nil {
			return nil, err
		}
		privKey = ecdsaKey
		keyType = "EC"
	default:
		return nil, fmt.Errorf("Unsupported key type")
	}
	if f, err := os.OpenFile(keyFile, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600); err != nil {
		log.Println("Failed to create key file", keyFile, ":", err)
		return nil, err
	} else {
		defer f.Close()
		if err = pem.Encode(f, &pem.Block{
			Type:  fmt.Sprintf("%s PRIVATE KEY", keyType),
			Bytes: der}); err != nil {

			log.Println("Failed to encode private key:", err)
			return nil, err
		}
	}
	return privKey, nil
}

// Verify that a self-signed certificate's signature is valid;
// that it was made by the private key corresponding to the public key
// in the certificate.
func VerifySelfSigned(c *x509.Certificate) error {
	return c.CheckSignature(c.SignatureAlgorithm, c.RawTBSCertificate, c.Signature)
}

// CreateSelfSigned generates a private key and self-signed public certificate.
func CreateSelfSigned(dn *pkix.Name, certFile string, keyFile string, options *KeyOptions) error {
	privKey, err := CreateKey(keyFile, options)
	if err != nil {
		return err
	}
	certTemplate, err := selfSignedTemplate(dn)
	if err != nil {
		log.Println("Failed to build self-signed template:", err)
		return err
	}
	var pubKey interface{}
	switch pk := privKey.(type) {
	case *rsa.PrivateKey:
		pubKey = &pk.PublicKey
		certTemplate.SignatureAlgorithm = x509.SHA512WithRSA
	case *ecdsa.PrivateKey:
		pubKey = &pk.PublicKey
		certTemplate.SignatureAlgorithm = x509.ECDSAWithSHA512
	default:
		return fmt.Errorf("Unsupported key type.")
	}
	certDer, err := x509.CreateCertificate(rand.Reader, certTemplate, certTemplate, pubKey, privKey)
	if err != nil {
		log.Println("Failed to create certificate:", err)
		return err
	}
	if f, err := os.Create(certFile); err != nil {
		log.Println("Failed to create certificate file", certFile, ":", err)
		return err
	} else {
		defer f.Close()
		if err = pem.Encode(f, &pem.Block{Type: "CERTIFICATE", Bytes: certDer}); err != nil {
			log.Println("Failed to encode certificate:", err)
			return err
		}
	}
	return nil
}
