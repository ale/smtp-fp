/*
   This file is part of gocert.

   gocert is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, version 3.

   gocert is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with gocert.  If not, see <http://www.gnu.org/licenses/>.
*/

package gocert

import (
	"crypto/x509"
	"encoding/pem"
	"io"
	"io/ioutil"
	"os"
)

func ShmTempDir(prefix string) (string, error) {
	return ioutil.TempDir(SHM_PATH, prefix)
}

func ShmTempFile(prefix string) (*os.File, error) {
	return ioutil.TempFile(SHM_PATH, prefix)
}

func WritePem(w io.Writer, preamble string, der []byte) error {
	return pem.Encode(w, &pem.Block{Type: preamble, Bytes: der})
}

func LoadX509Pem(certFile string) ([]*x509.Certificate, error) {
	f, err := os.Open(certFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	pemBytes, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	return X509Pem(pemBytes)
}

func X509Pem(pemBytes []byte) (certs []*x509.Certificate, err error) {
	var block *pem.Block
	for {
		block, pemBytes = pem.Decode(pemBytes)
		if block == nil {
			break
		}
		if block.Type != CERTIFICATE {
			continue
		}
		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			return certs, err
		}
		certs = append(certs, cert)
	}
	return certs, nil
}
