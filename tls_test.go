package smtpfp

import (
	"regexp"
	"strings"
	"testing"
	"time"
)

const (
	testValidServiceData = `
version: 1
config:
- valid_after: 2013-08-04 12:00
  domains:
   - domain: potager.org
     cert: certs/potager.org-2013.pem
   - domain: anargeek.net
     cert: certs/poivron.org-2013.pem
   - domain: example.org
     hidden_service: aaaaaaaaaaa.onion
- valid_after: 2014-07-01 12:00
  domains:
   - domain: potager.org
     cert: certs/potager.org-2014.pem
   - domain: anargeek.net
     cert: certs/poivron.org-2014.pem
   - domain: example.org
     hidden_service: aaaaaaaaaaa.onion
`

	testBadServiceData = `
version: 12
config:
- domains:
  - domain: something.com
`
)

func noWs(s string) string {
	rx := regexp.MustCompile(`[[:space:]]+`)
	return strings.TrimSpace(rx.ReplaceAllString(s, " "))
}

func TestConfig_New(t *testing.T) {
	service, err := NewServiceFromBytes("testdata/config", []byte(testValidServiceData))
	if err != nil {
		t.Fatal(err)
	}
	if service.Version != 1 {
		t.Fatalf("bad version: got=%d, want=%d", service.Version, 1)
	}
	if len(service.Config) != 2 {
		t.Fatalf("bad number of configs: %v", service)
	}
	if service.Config[0].Domains[0].Domain != "potager.org" {
		t.Fatalf("bad service config: %v", service)
	}

	// YAML serialized output should match the original (except
	// for whitespace).
	if noWs(service.String()) != noWs(testValidServiceData) {
		t.Fatalf("%+v", service)
	}
}

func TestConfig_ValidateOk(t *testing.T) {
	service, err := NewServiceFromBytes("testdata/config", []byte(testValidServiceData))
	if err != nil {
		t.Fatal(err)
	}

	if err := service.Validate(); err != nil {
		t.Fatal("validation error: ", err)
	}
}

func TestConfig_ValidateFail(t *testing.T) {
	service, err := NewServiceFromBytes("testdata/config", []byte(testBadServiceData))
	if err != nil {
		t.Fatal(err)
	}

	if err := service.Validate(); err == nil {
		t.Fatal("bad data passed validation: ", testBadServiceData)
	}
}

func TestConfig_Active(t *testing.T) {
	service, err := NewServiceFromBytes("testdata/config", []byte(testValidServiceData))
	if err != nil {
		t.Fatal(err)
	}

	c := service.Config[0]
	if c.IsActive(time.Date(2013, 1, 1, 0, 0, 0, 0, time.UTC)) {
		t.Error("config 0 is active on 2013/1/1")
	}
	if !c.IsActive(time.Date(2014, 1, 1, 0, 0, 0, 0, time.UTC)) {
		t.Error("config 0 is not active on 2014/1/1")
	}

	testData := []struct {
		ts         time.Time
		numResults int
	}{
		{time.Date(2013, 1, 1, 0, 0, 0, 0, time.UTC), 0},
		{time.Date(2014, 1, 1, 0, 0, 0, 0, time.UTC), 1},
		{time.Date(2015, 1, 1, 0, 0, 0, 0, time.UTC), 2},
	}
	for _, testCase := range testData {
		configs := service.ActiveConfigs(testCase.ts)
		got := len(configs)
		if want := testCase.numResults; got != want {
			t.Errorf("active services on %s: got=%d, want=%d -- %v", testCase.ts, got, want, configs)
		}
	}
}
