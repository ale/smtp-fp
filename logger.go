package smtpfp

import (
	"fmt"
	"log"
	"os"
)

var (
	logger     *Logger
	RootLogger *Logger
)

const (
	INFO = iota
	WARNING
	ERROR
)

func logLevelName(level int) string {
	switch level {
	case INFO:
		return "INFO"
	case WARNING:
		return "WARNING"
	case ERROR:
		return "ERROR"
	}
	return ""
}

type Logger struct {
	Level int
}

func (l *Logger) Warningf(format string, args ...interface{}) {
	l.Logf(WARNING, format, args...)
}

func (l *Logger) Infof(format string, args ...interface{}) {
	l.Logf(INFO, format, args...)
}

func (l *Logger) Info(args ...interface{}) {
	l.Logf(INFO, "%s", fmt.Sprintln(args...))
}

func (l *Logger) Errorf(format string, args ...interface{}) {
	l.Logf(ERROR, format, args...)
}

func (l *Logger) Error(args ...interface{}) {
	l.Logf(ERROR, "%s", fmt.Sprintln(args...))
}

func (l *Logger) Fatalf(format string, args ...interface{}) {
	l.Logf(ERROR, format, args...)
	os.Exit(1)
}

func (l *Logger) Fatal(args ...interface{}) {
	l.Logf(ERROR, "%s", fmt.Sprintln(args...))
	os.Exit(1)
}

func (l *Logger) Logf(level int, format string, args ...interface{}) {
	if level < l.Level {
		return
	}
	log.Printf("%s: %s", logLevelName(level), fmt.Sprintf(format, args...))
}

func init() {
	log.SetFlags(0)
	logger = &Logger{Level: WARNING}
	RootLogger = logger
}
