package smtpfp

import (
	"errors"
	"fmt"
	"path/filepath"
	"time"

	"git.autistici.org/ale/smtp-fp/output"
	"gopkg.in/yaml.v1"
)

var supportedProtocolVersions = []int{1}

func isProtocolVersionSupported(v int) bool {
	for _, sup := range supportedProtocolVersions {
		if v == sup {
			return true
		}
	}
	return false
}

// Validation results collect multiple errors, so we can show all the
// existing issues to the user at once. It satisfies the error
// interface.

// TLSDomain is a configuration for a specific domain. If you serve more
// than one certificate for a given domain, create multiple entries
// with the same Domain name.
type TLSDomain struct {
	Domain        string
	Cert          string `yaml:",omitempty"`
	HiddenService string `yaml:"hidden_service,omitempty"`
}

func (d TLSDomain) validate(res *manyerr) {
	if d.Domain == "" {
		res.Add(errors.New("missing 'domain'"))
	}
	if d.Cert == "" && d.HiddenService == "" {
		res.Add(errors.New("'cert' and 'hidden_service' can not both be empty"))
	}
}

// Configuration for a single service / certificate combination. It
// associates a bunch of domains with their certificates. It is
// possible, but not mandatory, to specify a validity time range.
type TLSConfig struct {
	ValidAfter *YamlTime `yaml:"valid_after,omitempty"`
	ValidUntil *YamlTime `yaml:"valid_until,omitempty"`
	Domains    []*TLSDomain
}

func (c *TLSConfig) validate(res *manyerr) {
	if len(c.Domains) == 0 {
		res.Add(errors.New("there are no domains in this config"))
	}
	if c.ValidAfter != nil && c.ValidUntil != nil && c.ValidAfter.Before(c.ValidUntil.Time) {
		res.Add(errors.New("the validity time range is not well-formed"))
	}
	for _, d := range c.Domains {
		d.validate(res)
	}
}

// IsActive returns true if the configuration is active at the
// specified time (if a validity time range is specified). Lacking a
// validity range, the configuration is assumed to be always valid.
func (c *TLSConfig) IsActive(now time.Time) bool {
	return (c.ValidAfter == nil || c.ValidAfter.Before(now)) && (c.ValidUntil == nil || c.ValidUntil.After(now))
}

// TLSService specifies which certificates are valid for a specific
// service. Configurations have a time-based validity, so more than
// one can be active at a given time. Use ActiveConfigs(time.Now()) to
// find those that currently apply.
//
// A TLSService is currently the only type that can be retrieved from
// a MetaConfig, but the code could easily be extended to support
// other types.
//
// TODO: finish decoupling the MetaConfig service getter.
//
type TLSService struct {
	// Directory containing this file. Certificate paths are
	// computed relative to here.
	Path string `yaml:"-"`

	// Schema version.
	Version int

	// Configurations.
	Config []*TLSConfig
}

func (s *TLSService) String() string {
	if d, err := yaml.Marshal(s); err == nil {
		return string(d)
	}
	return ""
}

// Validate verifies that the service configuration is correct.
func (s *TLSService) Validate() error {
	if !isProtocolVersionSupported(s.Version) {
		return errors.New("unsupported protocol version")
	}

	// The service must have at least some data in it.
	if len(s.Config) == 0 {
		return errors.New("the service is empty")
	}

	// Call validate() on all the contained objects and accumulate
	// their errors so that we can display them all together.
	var res manyerr
	for _, c := range s.Config {
		c.validate(&res)
	}

	// Ensure that all certificates are readable and well-formed.
	for _, crt := range s.getAllCertificates() {
		if _, err := s.GetCertificateFingerprint(crt); err != nil {
			res.Add(err)
		}
	}

	if res.Len() > 0 {
		return &res
	}
	return nil
}

// ActiveConfigs returns the currently active configurations, if any.
// Multiple values can be returned if the time ranges overlap.
func (s *TLSService) ActiveConfigs(now time.Time) []*TLSConfig {
	var active []*TLSConfig
	for _, c := range s.Config {
		if c.IsActive(now) {
			active = append(active, c)
		}
	}
	return active
}

// Returns a list of unique certificates referenced in the service configs.
func (s *TLSService) getAllCertificates() []string {
	tmp := make(map[string]struct{})
	for _, c := range s.Config {
		for _, d := range c.Domains {
			if d.Cert != "" {
				tmp[d.Cert] = struct{}{}
			}
		}
	}
	var certs []string
	for c := range tmp {
		certs = append(certs, c)
	}
	return certs
}

// Returns the full local path to a certificate. Certificate paths in
// the configuration are relative to the base directory of the
// configuration file.
func (s *TLSService) getCertificatePath(relPath string) string {
	if s.Path != "" {
		return filepath.Join(s.Path, relPath)
	}
	return relPath
}

func (s *TLSService) SetFile(filename string) {
	s.Path = filepath.Dir(filename)
}

// Parse a Service from its YAML definition.
func NewServiceFromBytes(filename string, data []byte) (*TLSService, error) {
	var s TLSService
	if err := yaml.Unmarshal(data, &s); err != nil {
		return nil, err
	}
	s.SetFile(filename)
	return &s, nil
}

// A map of domain -> unique strings.
type uniqueMap struct {
	tmp map[string]map[string]struct{}
}

func newUniqueMap() *uniqueMap {
	return &uniqueMap{
		tmp: make(map[string]map[string]struct{}),
	}
}

func (u *uniqueMap) Add(key, value string) {
	cur, ok := u.tmp[key]
	if !ok {
		cur = make(map[string]struct{})
		u.tmp[key] = cur
	}
	cur[value] = struct{}{}
}

func (u *uniqueMap) Result() map[string][]string {
	out := make(map[string][]string)
	for key, values := range u.tmp {
		var outvals []string
		for v := range values {
			outvals = append(outvals, v)
		}
		out[key] = outvals
	}
	return out
}

// GetTLSFingerprints returns a map of domain -> tls_fingerprint for all the
// active configurations.
func GetTLSFingerprints(services []*TLSService) map[string][]string {
	now := time.Now()
	fpmap := newUniqueMap()
	for _, s := range services {
		for _, c := range s.ActiveConfigs(now) {
			for _, d := range c.Domains {
				if d.Cert == "" {
					continue
				}
				fp, err := s.GetCertificateFingerprint(d.Cert)
				if err != nil {
					continue
				}
				fpmap.Add(d.Domain, fp)
			}
		}
	}
	return fpmap.Result()
}

// GetHiddenServices returns a domain -> hidden_service map for all the
// active configurations.
func GetHiddenServices(services []*TLSService) map[string][]string {
	now := time.Now()
	hsmap := newUniqueMap()
	for _, s := range services {
		for _, c := range s.ActiveConfigs(now) {
			for _, d := range c.Domains {
				if d.HiddenService == "" {
					continue
				}
				hsmap.Add(d.Domain, d.HiddenService)
			}
		}
	}
	return hsmap.Result()
}

// UpdateTLSService updates a service according to the configuration.
// The configuration must be Ready (i.e., keys must have been loaded
// by calling LoadKeys, and the repository must have been checked out
// locally by calling Download).
func UpdateTLSService(config *MetaConfig, service, outputSpec string, diff bool) error {
	gen, err := output.NewGenerator(outputSpec)
	if err != nil {
		return err
	}

	// Update the specified services.
	logger.Infof("updating service %s", service)
	services := config.GetTLSServices(service)
	fp := GetTLSFingerprints(services)
	if len(fp) == 0 {
		return fmt.Errorf("no valid TLS fingerprints for %s", service)
	}
	hs := GetHiddenServices(services)

	updates, err := gen.Update(fp, hs)
	if err != nil {
		return err
	}

	// Show a diff, or apply the updates, as requested.
	if diff {
		output.Diff(updates)
	} else {
		if err := output.Apply(updates); err != nil {
			return fmt.Errorf("error applying changes: %v", err)
		}
	}
	return nil
}
