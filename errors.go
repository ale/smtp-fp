package smtpfp

import (
	"fmt"
	"strings"
)

const errSeparator = " -- "

// Collect multiple errors, each identified by a string key.
type multierr struct {
	errs map[string]error
}

func (m *multierr) Len() int {
	return len(m.errs)
}

func (m *multierr) Add(tag string, err error) {
	if m.errs == nil {
		m.errs = make(map[string]error)
	}
	m.errs[tag] = err
}

func (m *multierr) Error() string {
	var tmp []string
	for tag, err := range m.errs {
		tmp = append(tmp, fmt.Sprintf("%s: %s", tag, err.Error()))
	}
	return strings.Join(tmp, errSeparator)
}

// Collect multiple errors in no particular order.
type manyerr struct {
	errs []error
}

func (m *manyerr) Len() int {
	return len(m.errs)
}

func (m *manyerr) Add(err error) {
	m.errs = append(m.errs, err)
}

func (m *manyerr) Error() string {
	var tmp []string
	for _, e := range m.errs {
		tmp = append(tmp, e.Error())
	}
	return strings.Join(tmp, errSeparator)
}
